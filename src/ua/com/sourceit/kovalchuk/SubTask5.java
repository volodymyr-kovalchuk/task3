package ua.com.sourceit.kovalchuk;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class SubTask5 {
    public static final String DELIMITER = " ";

    public static void main(String[] args) {
        printFormattedText("London is the capital of Great Britain", 20);
        System.out.println();
        printFormattedText("Argentina won the shootout 4-2 after a spectacular game which developed into the " +
                        "much-anticipated confrontation between the 35-year-old maestro Messi and his France " +
                        "opposite number Kylian Mbappe, who scored a hat-trick - the first in a World Cup final " +
                        "since 1966 - but still ended up on the losing side.",
                40);
    }

    private static void printFormattedText(String text, int charsInRow) {
        Queue<String> words = new ArrayDeque<>(Arrays.asList(text.split(DELIMITER)));
        splitIntoLines(charsInRow, words);
    }

    private static void splitIntoLines(int charsInRow, Queue<String> words) {
        while (!words.isEmpty()) {
            StringBuilder row = new StringBuilder(words.poll());
            splitByWordsInLine(charsInRow, words, row);
        }
    }

    private static void splitByWordsInLine(int charsInRow, Queue<String> words, StringBuilder row) {
        while (words.peek() != null && row.length() + words.peek().length() + 1 < charsInRow) {
            row.append(DELIMITER).append(words.poll());
        }
        System.out.println(row);
    }
}