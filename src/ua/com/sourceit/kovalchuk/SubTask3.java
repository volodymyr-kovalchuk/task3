package ua.com.sourceit.kovalchuk;

import java.util.*;
import java.util.stream.Collectors;

public class SubTask3 {
    public static void main(String[] args) {
        System.out.println(sortWords("ti tuytuit tut iti", 't'));
        System.out.println(sortWords("abb ab aabb", 'b'));
    }

    private static String sortWords(String text, char sortChar) {
        Comparator<String> wordsComparator = getStringComparator(sortChar);
        List<String> sortedWords = getSortedWords(text, wordsComparator);
        return String.join(" ", sortedWords);
    }

    private static List<String> getSortedWords(String text, Comparator<String> wordsComparator) {
        return Arrays.stream(text.split(" "))
                .sorted(wordsComparator)
                .collect(Collectors.toList());
    }

    private static Comparator<String> getStringComparator(char sortChar) {
        return (word1, word2) -> {
            int counter1 = 0, counter2 = 0;
            for (Character currentChar : word1.toCharArray()) {
                if (currentChar == sortChar) counter1++;
            }
            for (Character currentChar : word2.toCharArray()) {
                if (currentChar == sortChar) counter2++;
            }

            if (counter1 == counter2) {
                return word1.compareTo(word2);
            } else return counter1 - counter2;
        };
    }
}
