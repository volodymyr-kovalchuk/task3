package ua.com.sourceit.kovalchuk;

public class SubTask2 {
    public static void main(String[] args) {
        String text = "Good Golly Miss Molly";
        String encryptText = encryptText(text);
        String decryptText = decryptText(encryptText);
        System.out.println(encryptText);
        System.out.println(decryptText);
    }

    public static String encryptText(String text) {
        StringBuilder textPart1 = new StringBuilder(),
                textPart2 = new StringBuilder(),
                textPart3 = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            switch (i % 3) {
                case 0: {
                    textPart1.append(text.charAt(i));
                    break;
                }
                case 1: {
                    textPart2.append(text.charAt(i));
                    break;
                }
                case 2: {
                    textPart3.append(text.charAt(i));
                }
            }
        }
        return textPart1.append(textPart2).append(textPart3).toString();
    }

    public static String decryptText(String text) {
        int[] textPartsSize = getTextPartsSize(text);
        String[] textParts = getTextParts(text, textPartsSize);
        StringBuilder decryptedText = getDecryptedText(textPartsSize, textParts);
        return decryptedText.toString();
    }

    private static StringBuilder getDecryptedText(int[] textPartsSize, String[] textParts) {
        StringBuilder decryptedText = new StringBuilder();
        for (int i = 0; i < textPartsSize[0]; i++) {
            decryptedText.append(textParts[0].charAt(i));
            if (i < textPartsSize[1]) decryptedText.append(textParts[1].charAt(i));
            if (i < textPartsSize[2]) decryptedText.append(textParts[2].charAt(i));
        }
        return decryptedText;
    }

    private static String[] getTextParts(String text, int[] textPartsSize) {
        String[] textParts = new String[3];
        textParts[0] = text.substring(0, textPartsSize[0]);
        textParts[1] = text.substring(textPartsSize[0], textPartsSize[0] + textPartsSize[1]);
        textParts[2] = text.substring(textPartsSize[0] + textPartsSize[1]);
        return textParts;
    }

    private static int[] getTextPartsSize(String text) {
        int[] textParts = new int[3];
        switch (text.length() % 3) {
            case 0 : {
                textParts[0] = text.length() / 3;
                textParts[1] = text.length() / 3;
                textParts[2] = text.length() / 3;
                break;
            }
            case 1 : {
                textParts[0] = text.length() / 3 + 1;
                textParts[1] = text.length() / 3;
                textParts[2] = text.length() / 3;
                break;
            }
            case 2 : {
                textParts[0] = text.length() / 3 + 1;
                textParts[1] = text.length() / 3 + 1;
                textParts[2] = text.length() / 3;
            }
        }
        return textParts;
    }
}
