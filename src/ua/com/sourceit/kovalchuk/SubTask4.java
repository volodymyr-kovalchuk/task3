package ua.com.sourceit.kovalchuk;

public class SubTask4 {
    public static final String DELIMITER = " ";

    public static void main(String[] args) {
        System.out.println(changeWords("Good Golly Miss Molly"));
        System.out.println(changeWords("London is the capital of Great Britain"));
    }

    private static String changeWords(String text) {
        String[] words = text.split(DELIMITER);
        String[] outputWords = getOutputWords(words);
        return String.join(DELIMITER, outputWords);
    }

    private static String[] getOutputWords(String[] words) {
        String[] outputWords = new String[words.length];
        for (int i = 0; i < words.length; i++) {
            if (i == words.length - 1) outputWords[0] = words[i];
            else outputWords[i + 1] = words[i];
        }
        return outputWords;
    }
}
