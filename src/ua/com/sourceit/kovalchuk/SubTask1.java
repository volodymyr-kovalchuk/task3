package ua.com.sourceit.kovalchuk;

import java.util.*;
import java.util.stream.Collectors;

public class SubTask1 {
    public static void main(String[] args) {
        System.out.println(getLongestText("chy hio gtc yph pqx"));
        System.out.println(getLongestText("chy hio gtc ypc pqx"));
        System.out.println(getLongestText("chy hio gtc yph okc"));
        System.out.println(getLongestText("chy hio ukp"));
    }

    public static String getLongestText(String text) {
        List<String> words = Arrays.asList(text.split(" "));
        int maxNumberOfWords = 0;
        String maxWord = null;
        for (String word : words) {
            int numberOfWords = checkNumberOfWordsFromThisWord(word, words);
            if (numberOfWords > maxNumberOfWords) {
                maxNumberOfWords = numberOfWords;
                maxWord = word;
            }
        }
        return getWordsFromThisWord(maxWord, words);
    }

    private static int checkNumberOfWordsFromThisWord(String word, List<String> words) {
        int numberOfWords = 0;
        Map<Character, String> wordsFirstLetter = words.stream().collect(Collectors.toMap(
                w -> w.charAt(0), w -> w, (a, b) -> b));
        Map<String, Boolean> wordsUsing = words.stream().collect(Collectors.toMap(w -> w, w -> true, (a, b) -> b));

        String currentWord = word;
        while (wordsUsing.get(currentWord)) {
            numberOfWords++;
            wordsUsing.put(currentWord, false);
            char lastLetter = currentWord.charAt(currentWord.length() - 1);
            if (wordsFirstLetter.containsKey(lastLetter)) {
                currentWord = wordsFirstLetter.get(lastLetter);
            } else break;
        }
        return numberOfWords;
    }

    private static String getWordsFromThisWord(String word, List<String> words) {
        Map<Character, String> wordsFirstLetter = words.stream().collect(Collectors.toMap(
                w -> w.charAt(0), w -> w, (a, b) -> b));
        Map<String, Boolean> wordsUsing = words.stream().collect(Collectors.toMap(w -> w, w -> true, (a, b) -> b));

        StringBuilder result = new StringBuilder();
        String currentWord = word;
        while (wordsUsing.get(currentWord)) {
            if (!currentWord.equals(word)) result.append(" ");
            result.append(currentWord);
            wordsUsing.put(currentWord, false);
            char lastLetter = currentWord.charAt(currentWord.length() - 1);
            if (wordsFirstLetter.containsKey(lastLetter)) {
                currentWord = wordsFirstLetter.get(lastLetter);
            } else break;
        }
        return result.toString();
    }
}